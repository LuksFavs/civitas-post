import postUsuario from "./postUsuario.js";

/* get DOM elements */
const createBtn = document.querySelector("#btn");
const inputNome = document.querySelector("#nome");
const inputDataNas = document.querySelector("#nascimento");
const inputEmail = document.querySelector("#email");
const inputSenha = document.querySelector("#senha");
const errSpan = document.querySelector(".error");

/* (IIFE) - atualizar a tabela com as Chores recebidas pela API */
/*(async () => {
  await getChore(table);
})();
*/

/* disparar evento para criar nova Chore */
createBtn.addEventListener("click", async (event) => {
  event.preventDefault();
  const nome = inputNome.value;
  const data = inputDataNas.value;
  const email = inputEmail.value;
  const senha = inputSenha.value;
  /* verificar se os campos estão preenchidos */
  if (nome === "" || data === "" || email === "" || senha === "") {
    console.log("deu errado");
    alert("os campos não podem estar vazios");
  } 
  /* verifica formato do email*/
  else if (!email.includes("@") || !email.includes(".com")) {
    console.log("deu errado");
    alert("Erro de formato do email: coloque @ e .com ao final");
  }else {
    console.log("ta indo")
    await postUsuario(nome, data, email, senha);
    alert("Usuário cadastrado com sucesso");
  }
});
