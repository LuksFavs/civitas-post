/* requisição para criar um novo usuario no site */
const postUsuario = async (nome, data, email, senha) => {
  try {
    const response = await fetch("http://localhost:3000/cadastro", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        nome: `${nome}`,
        data: `${data}`,
        email: `${email}`,
        senha: `${senha}`,
      }),
    });
    const content = await response.json();
      console.log(content);
    return content;
  } catch (error) {
    console.log(error);
  }
};

export default postUsuario;
